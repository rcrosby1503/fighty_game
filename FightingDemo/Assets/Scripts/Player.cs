﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public Animator anim;
    public Rigidbody rbody;

    private float inputH;
    private float inputV;
    private bool run;
    private bool jump;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rbody = GetComponent<Rigidbody>();
        run = false;
        jump = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("1"))
        {
            anim.Play("WAIT01", -1, 0f); //exact name, base layer is -1 and 0f is the start of animation, 1f would be end
        }
        if (Input.GetKeyDown("2"))
        {
            anim.Play("WAIT02", -1, 0f); //exact name, base layer is -1 and 0f is the start of animation, 1f would be end
        }
        if (Input.GetKeyDown("3"))
        {
            anim.Play("WAIT03", -1, 0f); //exact name, base layer is -1 and 0f is the start of animation, 1f would be end
        }
        if (Input.GetKeyDown("4"))
        {
            anim.Play("WAIT04", -1, 0f); //exact name, base layer is -1 and 0f is the start of animation, 1f would be end
        }
        if (Input.GetMouseButtonDown(0))
        {
            int n = Random.Range(0, 2);

            if(n == 0)
            {
                anim.Play("DAMAGED00", -1, 0f);
            }
            else anim.Play("DAMAGED01", -1, 0f);
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            run = true;
        }
        else run = false;
        if (Input.GetKey(KeyCode.Space))
        {
            jump = true;
        }
        else jump = false;

        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");
        anim.SetFloat("inputH", inputH);
        anim.SetFloat("inputV", inputV);
        anim.SetBool("run", run);
        anim.SetBool("jump", jump);

        float moveX = inputH * 40f * Time.deltaTime;
        float moveZ = inputV * 50f * Time.deltaTime;

        if (moveZ <= 0) moveX = 0f;
        else if (run)
        {
            moveX *= 3f;
            moveZ *= 3f;
        }

        rbody.velocity = new Vector3(moveX,0f, moveZ);
    }
}
