﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Respawn : MonoBehaviour {

    private Vector3 startPos;
    private Quaternion startRot;

    public static int levelN = 0;


	// Use this for initialization
	void Start () {
        startPos = transform.position;
        startRot = transform.rotation;
	}
	
	//detect collision with collider trigger

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "death")
        {
            transform.position = startPos;
            transform.rotation = startRot;
            GetComponent<Animator>().Play("LOSE00", -1, 0f);
            GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
            GetComponent<Rigidbody>().angularVelocity = new Vector3(0f, 0f, 0f);
        }else if(col.tag == "checkpoint")
        {
            startPos = col.transform.position;
            startRot = col.transform.rotation;
            Destroy(col.gameObject);
        }else if(col.tag == "goal")
        {
            Destroy(col.gameObject);
            GetComponent<Animator>().Play("WIN00", -1, 0f);
            Invoke("nextLevel", 2f);
        }
    }

    void nextLevel()
    {
        levelN++;

        if(levelN > 1)
        {
            levelN = 0;
        }
        //Application.LoadLevel(levelN);
        SceneManager.LoadScene(levelN);
    }
}
